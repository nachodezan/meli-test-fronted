import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import SearchBox from "./components/SearchBox";
import Home from "./pages/Home";
import ItemList from "./pages/ItemList";
import ItemDetail from "./pages/ItemDetail";

const App = () => {
  const [searchTerm, setSearchTerm] = useState("");

  const handleSearch = (value) => {
    setSearchTerm(value);
  };

  return (
    <Router>
      <SearchBox handleSearch={handleSearch} />
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/items" exact>
          <ItemList term={searchTerm} />
        </Route>
        <Route path="/items/:id" exact component={ItemDetail} />
        <Redirect to="/" />
      </Switch>
    </Router>
  );
};

export default App;
