import { URL_API } from "../../config/constants";
import { rest } from "msw";

const searchParam = "Apple";
const idParam = "MLA918171240";

const item = {
  categories: [],
  items: [
    {
      id: idParam,
      title:
        "Macbook Air A1466 Silver 13.3 , Intel Core I5 5350u 8gb De Ram 128gb Ssd, Intel Hd Graphics 6000 1440x900px Macos Sierra 10",
      price: {
        currency: "ARS",
        amount: 149999,
        decimals: 2,
      },
      picture: "http://http2.mlstatic.com/D_722803-MLA42901955282_072020-I.jpg",
      condition: "new",
      free_shipping: true,
      address: "Buenos Aires",
    },
  ],
};

const itemDescription = {
  item: {
    id: "MLA918171240",
    title:
      "Macbook Air A1466 Silver 13.3 , Intel Core I5 5350u 8gb De Ram 128gb Ssd, Intel Hd Graphics 6000 1440x900px Macos Sierra 10",
    price: {
      currency: "ARS",
      amount: 149999,
      decimals: 2,
    },
    picture: "https://http2.mlstatic.com/D_722803-MLA42901955282_072020-O.jpg",
    condition: "new",
    free_shipping: true,
    sold_quantity: 200,
    description:
      'La notebook Apple MacBook Air A1466 es una solución tanto para trabajar y estudiar como para entretenerte. Al ser portátil, el escritorio dejará de ser tu único espacio de uso para abrirte las puertas a otros ambientes ya sea en tu casa o en la oficina.\n\nPantalla con gran impacto visual\nSu pantalla LED de 13.3" y 1440x900px de resolución te brindará colores más vivos y definidos. Tus películas y series preferidas cobrarán vida, ya que ganarán calidad y definición en cada detalle.\n\nEficiencia a tu alcance\nSu procesador Intel Core i5 de 2 núcleos, está pensado para aquellas personas generadoras y consumidoras de contenidos. Con esta unidad central, la máquina llevará a cabo varios procesos de forma simultánea, desde edición de videos hasta retoques fotográficos con programas profesionales.\n\nPotente disco sólido \nEl disco sólido de 128 GB hace que el equipo funcione a gran velocidad y por lo tanto te brinda mayor agilidad para operar con diversos programas.\n\nUn procesador exclusivo para los gráficos\nSu placa de video Intel HD Graphics 6000 convierte a este dispositivo en una gran herramienta de trabajo para cualquier profesional del diseño. Te permitirá lograr una gran performance en todos tus juegos y en otras tareas cotidianas que impliquen procesamiento gráfico.\n\nUna batería de larga duración\nLa batería de este equipo tiene una autonomía de alrededor de 12 horas. La duración varía según el uso, la configuración y otros factores, pero es ideal para quienes necesitan extender su jornada y seguir trabajando o estudiando con comodidad y sin cables.',
    categories: ["Computación", "Laptops y Accesorios", "Notebooks"],
  },
};

export const handlers = [
  rest.get(`${URL_API}/items/:id`, (req, res, ctx) => {
    const { id } = req.params;
    if (id === idParam) {
      return res(ctx.json(itemDescription));
    } else {
      return res(ctx.status(500));
    }
  }),
  rest.get(`${URL_API}/items`, (req, res, ctx) => {
    const query = req.url.searchParams;
    const q = query.get("q");

    if (q.toLowerCase() === searchParam.toLowerCase()) {
      return res(ctx.json(item));
    } else {
      return res(ctx.status(500));
    }
  }),
];
