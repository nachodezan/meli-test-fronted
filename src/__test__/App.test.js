import { setupServer } from "msw/node";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { handlers } from "./mocks/handlers";
import App from "../App";
import { PLACERHOLDER_SEARCH } from "../config/constants";

const server = setupServer(...handlers);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("loads and displays results", async () => {
  const searchTerm = "Apple";
  render(<App />);

  fireEvent.change(screen.getByPlaceholderText(PLACERHOLDER_SEARCH), {
    target: { value: searchTerm },
  });
  fireEvent.submit(screen.getByTestId("form"));

  await waitFor(() =>
    expect(screen.getByTestId("test-title")).toBeInTheDocument()
  );
});

test("navigate to description", async () => {
  const searchTerm = "Apple";
  render(<App />);

  fireEvent.change(screen.getByPlaceholderText(PLACERHOLDER_SEARCH), {
    target: { value: searchTerm },
  });
  fireEvent.submit(screen.getByTestId("form"));
  await screen.findByTestId("test-title");
  fireEvent.click(screen.getByTestId("test-title"));
  await waitFor(() =>
    expect(screen.getByTestId("test-box-item")).toBeInTheDocument()
  );
});
