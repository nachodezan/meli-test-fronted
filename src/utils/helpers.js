export const getPriceFormatted = ({ amount, decimals }) => {
  return amount.toLocaleString("es-ar", {
    style: "currency",
    currency: "ARS",
    minimumFractionDigits: decimals,
  });
};
