export const URL_API = "http://localhost:3001/api";
export const HOME_MESSAGE = "Realiza una busqueda";
export const NEW_CONDITION = "new";
export const PLACERHOLDER_SEARCH = "Nunca dejes de buscar";

export const ERROR_EMPTY_SEARCH = "Debe ingresar un algun contenido a buscar";
export const ERROR_SERVER = "Ocurrio un error, intente nuevamente mas tarde";
export const ERROR_RESULT_NOT_FOUND = "No se encontraron resultados";
