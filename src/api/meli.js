import { URL_API } from "../config/constants";

const get = (endpoint) => fetch(`${URL_API}${endpoint}`);

const api = { get };

export default api;
