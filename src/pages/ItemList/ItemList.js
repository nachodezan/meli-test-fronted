import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import meli from "../../api/meli";
import {
  ERROR_EMPTY_SEARCH,
  ERROR_SERVER,
  ERROR_RESULT_NOT_FOUND,
} from "../../config/constants";
import { Container } from "react-bootstrap";
import Message from "../../components/Message";
import Loading from "../../components/Loading";
import ItemCard from "../../components/ItemCard";
import Breadcrumb from "../../components/Breadcrumb";
import Seo from "../../components/Seo";

const ItemList = ({ term }) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [items, setItems] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    let isCancelled = false;
    const fetchData = async (term) => {
      setLoading(true);
      try {
        const response = await meli.get(`/items?q=${term}`);
        const { items, categories } = await response.json();
        if (!isCancelled) {
          setItems(items);
          setCategories(categories);
          setLoading(false);
        }
      } catch (e) {
        if (!isCancelled) {
          setLoading(false);
          setError(true);
          setErrorMessage(ERROR_SERVER);
        }
      }
    };

    if (term) {
      setError(false);
      fetchData(term);
    } else {
      setLoading(false);
      setError(true);
      setErrorMessage(ERROR_EMPTY_SEARCH);
    }

    return () => (isCancelled = true);
  }, [term]);

  const renderList = () =>
    items.map((item) => <ItemCard key={item.id} {...item} />);

  return (
    <>
      <Seo title={`${term}| MeLi Test`} description="Encontra lo que quieras" />
      {loading ? (
        <Loading />
      ) : error ? (
        <Message message={errorMessage} />
      ) : (
        <>
          {items.length > 0 ? (
            <Container fluid className="bg-container">
              <Breadcrumb categories={categories} />
              {renderList()}
            </Container>
          ) : (
            <Message message={ERROR_RESULT_NOT_FOUND} />
          )}
        </>
      )}
    </>
  );
};
export default ItemList;

ItemList.propTypes = {
  term: PropTypes.string,
};
