import { waitFor, render, screen } from "@testing-library/react";
import ItemList from "../ItemList";
import { ERROR_EMPTY_SEARCH, ERROR_SERVER } from "../../../config/constants";

describe("List of items test", () => {
  test("Should show empty error message", async () => {
    const searchTerm = "";
    render(<ItemList term={searchTerm} />);
    expect(screen.getByText(ERROR_EMPTY_SEARCH)).toBeTruthy();
  });

  test("Should show error server message", async () => {
    const searchTerm = "Apple";
    render(<ItemList term={searchTerm} />);
    await waitFor(() => expect(screen.getByText(ERROR_SERVER)).toBeTruthy());
  });
});
