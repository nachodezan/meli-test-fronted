import React from "react";
import Message from "../../components/Message";
import { HOME_MESSAGE } from "../../config/constants";
import Seo from "../../components/Seo";

const Home = () => {
  return (
    <>
      <Seo title="MercadoLibre" description="Encontra lo que quieras" />
      <Message message={HOME_MESSAGE} />
    </>
  );
};

export default Home;
