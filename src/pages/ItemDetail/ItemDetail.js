import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Container, Row, Col, Button } from "react-bootstrap";

import meli from "../../api/meli";
import { ERROR_SERVER, NEW_CONDITION } from "../../config/constants";
import { getPriceFormatted } from "../../utils/helpers";
import Message from "../../components/Message";
import Loading from "../../components/Loading";
import Breadcrumb from "../../components/Breadcrumb";
import Seo from "../../components/Seo";

const ItemDetail = () => {
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [itemData, setItemData] = useState();

  useEffect(() => {
    const fetchDetail = async (id) => {
      setLoading(true);
      try {
        const response = await meli.get(`/items/${id}`);
        if (!response.ok) throw new Error(response.statusText);
        const { item } = await response.json();
        setItemData(item);
        setLoading(false);
      } catch (e) {
        setLoading(false);
        setError(true);
      }
    };

    if (id) fetchDetail(id);
  }, [id]);

  return (
    <>
      {loading ? (
        <Loading />
      ) : error ? (
        <Message message={ERROR_SERVER} />
      ) : (
        itemData && (
          <>
            <Seo
              title={`${itemData.title || ""}| MeLi Test`}
              description="Encontra lo que quieras"
            />
            <Container
              fluid
              className="bg-container"
              data-testid="test-box-item"
            >
              <Breadcrumb categories={itemData.categories} />
              <Row className="justify-content-center">
                <Col md={8} className="bg-white">
                  <img
                    src={itemData.picture}
                    alt={itemData.title}
                    className="picture-item"
                  />
                </Col>
                <Col md={4} className="bg-white">
                  <p className="pt-32 f-14 text-muted">
                    {itemData.condition === NEW_CONDITION ? "Nuevo" : "Usado"}
                    {` - ${itemData.sold_quantity} vendidos`}
                  </p>
                  <h4 className="f-24 mb-0">{itemData.title}</h4>
                  <p className="f-46 mb-0 pt-32 pb-32">
                    {getPriceFormatted(itemData.price)}
                  </p>
                  <Button variant="primary" className="btn-block ">
                    Comprar
                  </Button>
                </Col>
              </Row>
              <Row className="pt-32 pl-32 pb-32 bg-white">
                <Col md={8}>
                  <h4 className="f-28 pb-32">Descripcion del producto</h4>
                  {itemData.description.split("\n").map((str, index) => (
                    <p key={`p-${index}`} className="f-16 text-muted">
                      {str}
                    </p>
                  ))}
                </Col>
              </Row>
            </Container>
          </>
        )
      )}
    </>
  );
};
export default ItemDetail;
