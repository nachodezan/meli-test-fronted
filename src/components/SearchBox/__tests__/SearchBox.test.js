import { render, fireEvent, screen, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter as Router } from "react-router-dom";

import SearchBox from "../SearchBox";
import { PLACERHOLDER_SEARCH } from "../../../config/constants";
import qs from "query-string";

test("Check empty input", async () => {
  const handleSearch = jest.fn();
  render(
    <Router>
      <SearchBox handleSearch={handleSearch} />
    </Router>
  );
  fireEvent.submit(screen.getByTestId("form"));
  expect(handleSearch).toHaveBeenCalledTimes(0);
});

test("Should be called search handler and change url", async () => {
  const handleSearch = jest.fn();
  const searchTerm = "Apple";
  render(
    <Router>
      <SearchBox handleSearch={handleSearch} />
    </Router>
  );
  fireEvent.change(screen.getByPlaceholderText(PLACERHOLDER_SEARCH), {
    target: { value: searchTerm },
  });
  fireEvent.submit(screen.getByTestId("form"));
  expect(handleSearch).toHaveBeenCalledTimes(1);

  await waitFor(() => {
    const { search } = qs.parse(window.location.search);
    expect(search).toEqual(searchTerm);
  });
});
