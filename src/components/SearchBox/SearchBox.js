import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import queryString from "query-string";
import { Navbar, Form, Button, InputGroup } from "react-bootstrap";
import { useHistory, useLocation } from "react-router-dom";
import searchButton from "../../assets/ic_Search.png";
import logoMeli from "../../assets/Logo_ML.png";
import { PLACERHOLDER_SEARCH } from "../../config/constants";

const SearchBox = ({ handleSearch }) => {
  const history = useHistory();
  const location = useLocation();

  const urlSearch = location.search;
  const { search } = queryString.parse(urlSearch);
  const [param, setParam] = useState(search || "");

  const doSearch = (value) => {
    if (value && value.trim() !== "") {
      handleSearch(value.toLowerCase());
      history.push(`/items?search=${value}`);
    }
  };

  const onSearch = (event) => {
    event.preventDefault();
    doSearch(param);
  };

  const goHome = () => {
    setParam("");
    history.push("/");
  };

  useEffect(() => doSearch(search), []);

  return (
    <Navbar className="navbar-custom justify-content-center">
      <Navbar.Brand>
        <img
          onClick={goHome}
          alt="logo"
          src={logoMeli}
          className="d-inline-block align-top"
        />
      </Navbar.Brand>
      <Form inline onSubmit={onSearch} data-testid="form">
        <Form.Group controlId="formSearch">
          <InputGroup>
            <input
              id="search"
              name="search"
              type="text"
              className="input-search form-control f-18"
              placeholder={PLACERHOLDER_SEARCH}
              value={param}
              onChange={(e) => setParam(e.target.value)}
            />

            <Button type="submit" className="btn-search">
              <img src={searchButton} alt="search icon" />
            </Button>
          </InputGroup>
        </Form.Group>
      </Form>
    </Navbar>
  );
};

export default SearchBox;

SearchBox.propTypes = {
  handleSearch: PropTypes.func,
};
