import React from "react";
import PropTypes from "prop-types";
import { Breadcrumb as BreadcrumbBoot } from "react-bootstrap";

const Breadcrumb = ({ categories }) => {
  return (
    <BreadcrumbBoot listProps={{ className: "custom-breadcrumb pt-16 pb-16" }}>
      {categories.map((category, index) => (
        <BreadcrumbBoot.Item key={`${category}${index}`} href="#">
          {category}
        </BreadcrumbBoot.Item>
      ))}
    </BreadcrumbBoot>
  );
};
export default Breadcrumb;

Breadcrumb.propTypes = {
  categories: PropTypes.array,
};
