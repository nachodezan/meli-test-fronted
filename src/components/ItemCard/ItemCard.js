import React from "react";
import PropTypes from "prop-types";
import { Row, Col } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import shippingLogo from "../../assets/ic_shipping.png";
import { getPriceFormatted } from "../../utils/helpers";

const ItemCard = ({ id, title, price, free_shipping, address, picture }) => {
  const history = useHistory();

  const navigateToItem = (id) => history.push(`items/${id}`);

  return (
    <Row className="justify-content-center bg-white pb-16 pt-16 border-bottom">
      <Col md={9}>
        <Row>
          <Col md={3}>
            <img
              src={picture}
              alt={title}
              className="thumbnail"
              onClick={() => navigateToItem(id)}
            />
          </Col>

          <Col md={9}>
            <h4 className="f-24 mb-0 pt-16">
              {getPriceFormatted(price)}
              {free_shipping && <img src={shippingLogo} alt="free shipping" />}
            </h4>

            <p
              className="pointer f-18 pt-32"
              data-testid="test-title"
              onClick={() => navigateToItem(id)}
            >
              {title}
            </p>
          </Col>
        </Row>
      </Col>
      <Col md={3} className="pt-16 pl-16">
        <p className="f-12"> {address}</p>
      </Col>
    </Row>
  );
};

export default ItemCard;

ItemCard.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  price: PropTypes.object,
  free_shipping: PropTypes.bool,
  address: PropTypes.string,
  picture: PropTypes.string,
};
