import { fireEvent, waitFor, render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";

import ItemCard from "../ItemCard";

const data = {
  id: "MLA804207795",
  title: "iPod Touch 32 Gb",
  free_shipping: true,
  price: {
    currency: "ARS",
    amount: 32999,
    decimals: 2,
  },
  picture: "http://http2.mlstatic.com/D_684489-MLA32011256128_082019-O.jpg",
};

describe("Description of items", () => {
  test("Should navigate when click image", async () => {
    render(
      <Router>
        <ItemCard {...data} />
      </Router>
    );

    fireEvent.click(screen.getByAltText(data.title));
    await waitFor(() => {
      expect(window.location.pathname).toEqual(`/items/${data.id}`);
    });
  });
});
