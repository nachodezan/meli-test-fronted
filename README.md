# MercadoLibre Fronted Test

Esta aplicacion permite realizar busquedas de productos, obteniendo un listado y luego pudiendo al detalle de cada uno.

## Requerimientos
 * ReactJS v17
 * npm v7.8

## Como correr el proyecto
En la carpeta raiz del proyecto, se puede correr:

### `npm start`

Esto corre la aplicacion en modo desarrollo.\
Abrir [http://localhost:3000](http://localhost:3000) para verlo en el navegador.

## Testing
Para correr los tests, parado en la carpeta raiz del proyecto:
### `npm test`

## Librerias
* ReactBootstrap (UI)
* React Router DOM
* NodeSass
* React Helmet
* React Testing Library

## Mejoras
* Busqueda dinamica implementando alguna tecnica de debounce.
* Migrar a Typescript.
* Crear scss por componente.
* Agregar Styled Components

## Agradecimientos
Gracias MercadoLibre por la oportunidad.